<?php

// can be 'ergol' or 'gemserv'
define('CONFIG_TYPE', 'ergol');

// change this if this file is not in a subdir of ergol root directory
// See ergol.json description on
// https://codeberg.org/adele.work/ergol#configuration-ergol-json
define('CONFIG_PATH', __DIR__."/../ergol.json");
